################################################################################
#File: z:\Developer\Powershell\Choco_Install.ps1                               #
#Project: z:\Developer\Powershell                                              #
#Created Date: Thursday June 10th 2021                                         #
#Author: Gian Vetter                                                           #
#-----                                                                         #
#Last Modified: Thursday June 10th 2021 5:13:59 pm                             #
#Modified By: the developer formerly known as Gian Vetter                      #
#-----                                                                         #
#Copyright (c) <<2021>> <<Gians-IT>>                                           #
#-----                                                                         #
#HISTORY:                                                                      #
################################################################################



$Packages = 'adobereader', 'winrar', 'googlechrome', 'notepadplusplus.install', 'microsoft-windows-terminal', 'visualstudio-installer', 'winscp', 'vscode'


# Check if Choco is installed
if (!(Get-Command chocolatey.exe -ErrorAction SilentlyContinue | Out-Null)) {
    Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
}


#Install the apps
foreach ($PackageName in $Packages) {
    choco install $PackageName -y
}


#Update the apps
foreach ($PackageName in $Packages) {
    choco upgrade $PackageName -y
}
